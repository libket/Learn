{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "[![LibKet](../images/LibKet.png)](https://gitlab.com/libket/LibKet)\n",
        "**LibKet - The Quantum Expression Template Library.**\n",
        "- Repository:    https://gitlab.com/libket/LibKet/\n",
        "- Documentation: https://libket.readthedocs.io/\n",
        "- API docs:      https://libket.gitlab.io/LibKet/\n",
        "\n",
        "***"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "# Tutorial \\#3: Hands-on Scientific Computing with LibKet - Part 2\n",
        "\n",
        "In this tutorial you will learn to\n",
        "1. write quantum expressions with for-loops\n",
        "2. build quantum algorithms from components"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Getting started\n",
        "Let's include **LibKet**'s main headerfile, import its namespaces, and inject the code for displaying images. This can take some time, stay tuned."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "#include \"LibKet.hpp\"\n",
        "using namespace LibKet;\n",
        "using namespace LibKet::circuits;\n",
        "using namespace LibKet::filters;\n",
        "using namespace LibKet::gates;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "A common building block in many quantum algorithms is the [Quantum Fourier Transform](https://en.wikipedia.org/wiki/Quantum_Fourier_transform) (QFT) and its inverse QFT$^\\dagger$\n",
        "\n",
        "![QFT](../images/qft_circuit.png)\n",
        "\n",
        "where **CRk** denotes the **controlled phase shift** gate, ``crk<k>(ctrl, target)``, with angle $\\theta = \\pi/2^k$ (radians). This gate is particularly useful for implementing the QFT as the rotation angles vary with the distance between the control and the target qubit."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**LibKet** has ready-to-use quantum expressions ``qft(...)`` and ``qftdag(...)``. Let's start with the former one."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "auto expr = LibKet::circuits::qft<LibKet::circuits::QFTMode::noswap>(init());\n",
        "\n",
        "QDevice<QDeviceType::cirq_simulator, 4> cirq;\n",
        "std::cout << cirq(expr) << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The pre-implemented ``qft<mode>(...)`` function makes use of another advanced **LibKet** feature."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Compile-time for loops with ``static_for()``\n",
        "\n",
        "The generic interface of the ``static_for()`` function reads\n",
        "```cpp\n",
        "template<index_t for_start,\n",
        "         index_t for_end,\n",
        "         index_t for_step,\n",
        "         template<index_t start, index_t end, index_t step, index_t index>\n",
        "         class functor,\n",
        "         typename functor_return_type,\n",
        "         typename... functor_types>\n",
        "inline auto\n",
        "static_for(functor_return_type&& functor_return_arg,\n",
        "           functor_types&&... functor_args)\n",
        "```\n",
        "Let's start with a small\n",
        "####  Example\n",
        "We first create a **functor**, which represents the loop's body"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "template<index_t start, index_t end, index_t step, index_t index>\n",
        "struct ftor \n",
        "{\n",
        "    template<typename  Expr>\n",
        "    inline constexpr auto operator()(Expr&& expr) noexcept \n",
        "    {\n",
        "        // Returns the controlled phase shift gate with angle\n",
        "        // theta = pi/2^(index+1) between qubits index and index+1\n",
        "        return crk<index+1>(sel<index>  (gototag<0>()),\n",
        "                            sel<index+1>(gototag<0>(expr))\n",
        "                           );\n",
        "    }\n",
        "};"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "To *loop* through this functor (at compile time) we call the ``utils::static_for<start, end, step, body>(...)`` function as follows. Note the usefulness of the ``tag``/``gototag`` mechanism to restore the original filter settings easily"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "auto expr = utils::static_for<0,4,1,ftor>(tag<0>(init()));\n",
        "\n",
        "QDevice<QDeviceType::cirq_simulator, 4> cirq;\n",
        "std::cout << cirq(expr) << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "#### Nested loops\n",
        "Compile-time for loops can even be nested as it is done in our QFT implementation.\n",
        "\n",
        "Here, the outer loop (``qft_loop_outer``) repeatedly calls the inner loop (``qft_loop_inner``) with different values of the ``start`` index"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "template<index_t start, index_t end, index_t step, index_t index>\n",
        "struct qft_loop_inner\n",
        "{\n",
        "    template<typename Expr0, typename Expr1>\n",
        "    inline constexpr auto operator()(Expr0&& expr0, Expr1&& expr1) noexcept\n",
        "    {\n",
        "        return crk<index-start+2>(sel<index  >(gototag<0>(expr0)),\n",
        "                                  sel<start-1>(gototag<0>(expr1))\n",
        "                                 );\n",
        "    }\n",
        "};\n",
        "\n",
        "template<index_t start, index_t end, index_t step, index_t index>\n",
        "struct qft_loop_outer\n",
        "{\n",
        "    template<typename Expr0, typename Expr1>\n",
        "    inline constexpr auto operator()(Expr0&& expr0, Expr1&& expr1) noexcept\n",
        "    {\n",
        "        return utils::static_for<index+1, end, 1, qft_loop_inner>\n",
        "            (h(sel<index>(gototag<0>(expr0))),\n",
        "             gototag<0>(expr1)\n",
        "            );\n",
        "    }\n",
        "};"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Armed with the nested compile-time for-loop we can implement the expression for the QFT as a one-liner"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "auto expr = utils::static_for<0,3,1,qft_loop_outer>(tag<0>(init()), tag<0>());\n",
        "\n",
        "QDevice<QDeviceType::cirq_simulator, 4> cirq;\n",
        "std::cout << cirq(expr) << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "What happens if you move the ``init()`` from the first to the second argument?"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": []
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "#### Execise: ``allswap()``\n",
        "Now it's your turn. Write a functor and a compile-time for-loop that ``swap``s $\\lvert q_0\\rangle$ with $\\lvert q_n\\rangle$, $\\lvert q_1\\rangle$ with $\\lvert q_{n-1}\\rangle$, etcetera. \n",
        "\n",
        "Apply your implementation *after* the QFT and compare the generated quantum assembly code with our reference implementation\n",
        "```cpp\n",
        "auto expr = LibKet::circuits::qft(...);\n",
        "```\n",
        "or\n",
        "```cpp\n",
        "auto expr = LibKet::circuits::allswap(LibKet::circuits::qft<LibKet::circuits::QFTMode::noswap>(...));\n",
        "```"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": []
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "C++14",
      "language": "C++14",
      "name": "xcpp14"
    },
    "language_info": {
      "codemirror_mode": "text/x-c++src",
      "file_extension": ".cpp",
      "mimetype": "text/x-c++src",
      "name": "c++",
      "version": "14"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 1
}
