{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "[![LibKet](../images/LibKet.png)](https://gitlab.com/libket/LibKet)\n",
        "**LibKet - The Quantum Expression Template Library.**\n",
        "- Repository:    https://gitlab.com/libket/LibKet/\n",
        "- Documentation: https://libket.readthedocs.io/\n",
        "- API docs:      https://libket.gitlab.io/LibKet/\n",
        "\n",
        "***"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "# Tutorial \\#2: Hands-on Scientific Computing with LibKet - Part 1\n",
        "\n",
        "In this tutorial you will learn to\n",
        "1. offload quantum expressions to different quantum devices\n",
        "2. customize the offloading pipeline\n",
        "3. implement 3-qubit Grover's algorithm"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Getting started\n",
        "\n",
        "Let's include **LibKet**'s main headerfile and import its namespaces. This can take some time, stay tuned."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "#include \"LibKet.hpp\"\n",
        "using namespace LibKet;\n",
        "using namespace LibKet::circuits;\n",
        "using namespace LibKet::filters;\n",
        "using namespace LibKet::gates;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Let us create a simple quantum expression for the first Bell state\n",
        "\n",
        "![Bell state](../images/multi_qubit_circuit_HI_CNOT.png)\n",
        "\n",
        "and evaluate it on different quantum backends. Don't forget to ``measure`` at the end since some backends do not support direct statevector readouts."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "auto expr = measure(cnot(h(sel<0>()), sel<1>(init())));"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Cirq\n",
        "\n",
        "![Cirq](../images/cirq_logo.png)\n",
        "\n",
        "[Cirq](https://quantumai.google/cirq) is a Python package for writing, manipulating, and optimizing quantum circuits developed by Google. It is seamlessly integrated into **LibKet** using the embedded Python interpreter, so you should not notice all the technical details going on under the hood.\n",
        "\n",
        "As for the [QuEST](https://quest.qtechtheory.org) simulator, we simply create a quantum device for two qubits, load the quantum expression into it, and evaluate it."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "QDevice<QDeviceType::cirq_simulator, 2> cirq;\n",
        "utils::json result = cirq(expr).eval(1);"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Unlike QuEST, Cirq returns the outcome of the evaluation as JSON object which we can print directly"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "std::cout << result << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "We can also get a slightly prettier output"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "std::cout << result.dump(2) << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "As this is still not too informative, **LibKet** provides quick access `get<...>()` functions to retrieve information about the ``duration`` of the quantum computation, a ``histogram`` of all measured states, and the ``best`` state, i.e. the one that was measured most often"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "std::cout << \"duration  : \" << cirq.get<QResultType::duration>(result).count() << \" seconds\" << std::endl;\n",
        "std::cout << \"histogram : \" << cirq.get<QResultType::histogram>(result)        << std::endl;\n",
        "std::cout << \"best      : \" << cirq.get<QResultType::best>(result)             << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Let's increase the number of **shots**, i.e. the number of times we run the quantum circuit in our simulator, to ``20`` to see how the histogram changes "
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "result = cirq.eval(20);\n",
        "\n",
        "std::cout << \"duration  : \" << cirq.get<QResultType::duration>(result).count() << \" seconds\" << std::endl;\n",
        "std::cout << \"histogram : \" << cirq.get<QResultType::histogram>(result)        << std::endl;\n",
        "std::cout << \"best      : \" << cirq.get<QResultType::best>(result)             << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Quantum Inspire\n",
        "\n",
        "![QuantumInspire](../images/quantuminspire_logo.png)\n",
        "\n",
        "Let's move on to the next quantum backend supported by **LibKet** - [QuantumInspire](https://quantuminspire.com), which is a multi hardware Quantum Technology platform by QuTech. Without any changes to the quantum expression ``expr``, we create a new Quantum Inspire device, populate it with the quantum expression and run it the same way as before. The only difference compared to the Cirq backend is that we need to provide a valid username and password. This can be obtained by creating a free account [here](https://quantuminspire.com/account/create)."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "QDevice<QDeviceType::qi_26_simulator, 2> qi(\"FILL_USERNAME_HERE\", \"FILL_PASSWORD_HERE\");\n",
        "utils::json result = qi(expr).eval(20);\n",
        "\n",
        "std::cout << \"duration  : \" << qi.get<QResultType::duration>(result).count() << \" seconds\" << std::endl;\n",
        "std::cout << \"histogram : \" << qi.get<QResultType::histogram>(result)        << std::endl;\n",
        "std::cout << \"best      : \" << qi.get<QResultType::best>(result)             << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Rigetti\n",
        "\n",
        "![Rigetti](../images/rigetti_logo.png)\n",
        "\n",
        "Our third backend, [Rigetti](https://www.rigetti.com), requires some additional configuration as it relies on a quantum virtual machine ([qvm](https://github.com/rigetti/qvm)) and a quantum compiler toolchain ([quilc](https://github.com/rigetti/quilc)). Both tools can be [installed](https://pyquil-docs.rigetti.com/en/stable/start.html) locally or on remote servers. Thanks to our partner [Stillwater Supercomputing, Inc.](http://www.stillwater-sc.com) we have two virtual servers running the pre-built [docker images](https://hub.docker.com/u/rigetti) during the tutorial session.\n",
        "\n",
        "The easiest way to customize quantum devices is by means of the JSON-based constructor"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "utils::json config = { \n",
        "    { \"qvm_url\",   \"http://159.223.8.136:80\"  },\n",
        "    { \"quilc_url\", \"tcp://159.223.8.38:80\" }\n",
        "};\n",
        "\n",
        "QDevice<QDeviceType::rigetti_9q_square_simulator, 2> rigetti(config);\n",
        "utils::json result = rigetti(expr).eval(20);\n",
        "\n",
        "std::cout << \"duration  : \" << rigetti.get<QResultType::duration>(result).count() << \" seconds\" << std::endl;\n",
        "std::cout << \"histogram : \" << rigetti.get<QResultType::histogram>(result)        << std::endl;\n",
        "std::cout << \"best      : \" << rigetti.get<QResultType::best>(result)             << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Let's take a short look under the hood to see how much work is required to write low-level quantum assembly language (QASM) for the three simulators"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "std::cout << \"------- Cirq -------\\n\" << cirq    << std::endl;\n",
        "std::cout << \"-- QuantumInspire --\\n\" << qi  << std::endl;\n",
        "std::cout << \"------ Rigetti -----\\n\" << rigetti << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "**LibKet** is designed as backend-agnostic quantum programming framework with a unified API, which means that the core functionality is implemented for all quantum backends. If a particular backend does not support a specific function, e.g., Cirq does not report the ``duration`` of the quantum computation, default values are returned.\n",
        "\n",
        "Some quantum backends provide extra functionality, which is exposed via **LibKet**. Cirq for instance can export the quantum circuit to [LaTeX](https://www.latex-project.org) code using the [Qcircuit](https://ctan.org/pkg/qcircuit) package"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "std::cout << cirq.to_latex() << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "The resulting circuit looks as follows\n",
        "\n",
        "![Bell state](../images/qiskit_to_latex.png)"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Computation offloading\n",
        "\n",
        "**LibKet**'s computation offloading model is very similar to that of [CUDA](https://developer.nvidia.com/cuda-downloads) to ease the transition from GPU- to quantum-accelerated computing. The `device.eval(...)` is just one of three ways to run a quantum expression, which we will refer to as **quantum kernel**, on a quantum device.\n",
        "\n",
        "#### ``LibKet::utils::json device.eval(...)`` \n",
        "offloads the quantum computation to the quantum device and returns the evaluated result as JSON object once the quantum computation has completed. Exceptions are the QuEST and QX simulators where a reference to the internal state vector is returned.\n",
        "\n",
        "#### ``LibKet::QJob* device.execute(...)`` \n",
        "offloads the quantum computation to the quantum device and returns a ``QJob`` pointer once the quantum computation has completed.\n",
        "\n",
        "#### ``LibKet::QJob* device.execute_async(...)`` \n",
        "offloads the quantum computation to the quantum device and returns a ``QJob`` pointer immediately.\n",
        "\n",
        "All three methods have the same interface. For Python-based quantum backends this is\n",
        "```cpp\n",
        "QJob<QJobType::Python>* execute(std::size_t shots                 = [default from ctor],\n",
        "                                const std::string& script_init    = \"\",\n",
        "                                const std::string& script_before  = \"\",\n",
        "                                const std::string& script_after   = \"\",\n",
        "                                QStream<QJobType::Python>* stream = NULL)\n",
        "```\n",
        "and for backends implemented in C/C++ like QuEST and QX this is\n",
        "```cpp\n",
        "QJob<QJobType::CXX>*    execute(std::size_t                         shots       = [default from ctor],\n",
        "                                std::function<void(QDevice_QuEST*)> ftor_init   = NULL,\n",
        "                                std::function<void(QDevice_QuEST*)> ftor_before = NULL,\n",
        "                                std::function<void(QDevice_QuEST*)> ftor_after  = NULL,\n",
        "                                QStream<QJobType::CXX>*             stream      = NULL)\n",
        "```"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Let's change ``cirq.eval(20)`` into ``cirq.execute_async(20)`` to offload the execution of the quantum kernel to the ``cirq`` device. Since this cloud-based environment does not allow non-blocking execution this and ``cirq.execute(20)`` behave identically. If used in a regular program, asynchronous execution immediately returns the scope to the host program and passes a pointer to the ``QJob`` object."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "auto job = cirq.execute_async(20);"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "``QJob`` objects support the following functionality\n",
        "\n",
        "#### ``QObj* wait()`` \n",
        "waits for the job to complete (blocking)\n",
        "#### ``bool query()`` \n",
        "returns ``true`` if the job completed and ``false`` otherwise (non-blocking)\n",
        "#### ``utils::json get()`` \n",
        "returns the result as JSON object after completion (blocking)\n",
        "\n",
        "Let's collect the result of our quantum kernel execution"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "result = job->get();\n",
        "std::cout << result.dump(2) << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Customizing quantum kernel execution\n",
        "\n",
        "The optional hooks ``script_init``, ``script_before``, and ``script_after`` and their C++ counterparts ``ftor_init``, ``ftor_before``, and ``ftor_after`` make it possible to inject user-defined code at three different locations of the execution process:\n",
        "\n",
        "#### ``script_init``\n",
        "is performed before any other code of the execution process. It can be used for importing additional Python modules\n",
        "#### ``script_before``\n",
        "is performed just before sending the instructions to the quantum device. It can be used to pre-process the quantum circuit, e.g., to perform user-specific optimizations on the raw quantum circuit, before it runs through the backend-specific pipeline\n",
        "#### ``script_after``\n",
        "is performed just after receiving the result from the quantum device. It can be used to post-process the raw results received from the quantum device, e.g., to generate histograms or other types of visualizations\n",
        "\n",
        "Let's inject a simple statement after the execution that collects the histogram data of the experiment using Qiskit's [``get_count()``](https://qiskit.org/documentation/stubs/qiskit.result.Result.html#qiskit.result.Result.get_counts) function"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "trusted": true
      },
      "outputs": [],
      "source": [
        "auto job = qi.execute_async(20,\n",
        "                            /* init_script   */\n",
        "                            \"\",\n",
        "                            /* before_script */\n",
        "                            \"\",\n",
        "                            /* after_script */\n",
        "                            \"return json.dumps(result['histogram'])\\n\"\n",
        "                            );\n",
        "std::cout << job->get().dump(2) << std::endl;"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "It should be noted that the code injections are idented automatically and must not have trailing ``\\t``'s. Each line must end with ``\\n``."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Example: 3-qubit Grover's algorithm\n",
        "\n",
        "[Grover's algorithm](https://en.wikipedia.org/wiki/Grover%27s_algorithm) is one of the most prominent quantum algorithms that can be found in many standard textbook on quantum computing. We will not discuss the circuit here but simply provide a pre-implemented expression that 'finds' the binary number 010 (decimal 2) with high probability.\n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {},
      "outputs": [],
      "source": [
        "// Black-box oracle operator for selecting the binary 010 (decimal 2)\n",
        "auto oracle = [](auto expr) \n",
        "{ \n",
        "    return x(sel_<0>(x(h(sel_<2>(ccnot(sel_<0>(), sel_<1>(), sel_<2>(h(x(sel_<2>(x(sel_<0>(expr)))))))))))); \n",
        "};\n",
        "\n",
        "// Grover's diffusion operator\n",
        "auto diffusion = [](auto expr) \n",
        "{\n",
        "    return h(x(all(h(sel_<2>(ccnot(sel_<0>(), sel_<1>(), sel_<2>(h(sel_<2>(x(h(all(expr)))))))))))); \n",
        "};\n",
        "\n",
        "// Grover's algorithm with a single iteration\n",
        "auto expr = measure(diffusion(oracle(h(init()))));\n",
        "\n",
        "QDevice<QDeviceType::qi_26_simulator, 3> qi(\"FILL_USERNAME_HERE\", \"FILL_PASSWORD_HERE\");\n",
        "auto job = qi(expr).execute_async(1024,\n",
        "                                  \"\",\n",
        "                                  \"\",\n",
        "                                  \"return json.dumps(result['histogram'])\\n\"\n",
        "                                  );\n",
        "std::cout << job->get().dump(2) << std::endl;"
      ]
    }
  ],
  "metadata": {
    "kernelspec": {
      "display_name": "C++14",
      "language": "C++14",
      "name": "xcpp14"
    },
    "language_info": {
      "codemirror_mode": "text/x-c++src",
      "file_extension": ".cpp",
      "mimetype": "text/x-c++src",
      "name": "c++",
      "version": "14"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 1
}
